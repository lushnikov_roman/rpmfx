package quickSort;

import java.util.Arrays;

/**
 * класс, демонстрирующий быструю сортировку
 */
public class Sort {
    public static void main(String[] args) {
        int array[] = {2, 5, 3, 0, -1};
        int low=0;
        int high=array.length-1;
        System.out.println(Arrays.toString(Sort(array,low,high)));
    }

    private static int[] Sort(int array[], int low, int high) {
        if (array.length == 0) {
            return array;
        }
        if (low >= high) {
            return array;
        }
        int middle = low + (high - low) / 2;
        int opora = array[middle];
        int i = low;
        int j = high;
        while (i <= j) {
            while (array[i] < opora) {
                i++;
            }
            while (array[j] > opora) {
                j--;
            }
            if (i <= j) {
                int temp = array[i];
                array[i] = array[j];
                array[j] = temp;
                i++;
                j--;
            }
        }
        if (low < j)
            Sort(array, low, j);
        if (high > i) {
            Sort(array, i, high);
        }
        return array;
        //System.out.println(Arrays.toString(array));
    }

}
