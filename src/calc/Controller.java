package calc;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import machine.animation.Shake;

import java.util.ArrayList;
import java.util.Arrays;

@SuppressWarnings("unused")
public class Controller {

    @FXML
    private Button division;

    @FXML
    private Button zero;

    @FXML
    private Button one;

    @FXML
    private Button two;

    @FXML
    private Button three;

    @FXML
    private Button four;

    @FXML
    private Button five;

    @FXML
    private Button six;

    @FXML
    private Button seven;

    @FXML
    private Button eight;

    @FXML
    private Button nine;

    @FXML
    private double result;

    @FXML
    private Button multiply;

    @FXML
    private Button substract;

    @FXML
    private Button addition;

    @FXML
    private TextField window;

    @FXML
    private Button c;

    @FXML
    private Button point;

    /**
     * Методы ввода чисел или знаков операций
     */

    @FXML
    void clear(ActionEvent event) {
        window.clear();
    }

    @FXML
    void point(ActionEvent event) {
        window.setText(window.getText() + ".");
    }

    @FXML
    void addition(ActionEvent event) {
        window.setText(window.getText() + "+");
    }

    @FXML
    void eight(ActionEvent event) {
        window.setText(window.getText() + 8);
    }

    @FXML
    void five(ActionEvent event) {
        window.setText(window.getText() + 5);
    }

    @FXML
    void four(ActionEvent event) {
        window.setText(window.getText() + 4);
    }

    @FXML
    void multiply(ActionEvent event) {
        window.setText(window.getText() + "*");
    }

    @FXML
    void nine(ActionEvent event) {
        window.setText(window.getText() + 9);
    }

    @FXML
    void one(ActionEvent event) {
        window.setText(window.getText() + 1);
    }

    @FXML
    void seven(ActionEvent event) {
        window.setText(window.getText() + 7);
    }

    @FXML
    void six(ActionEvent event) {
        window.setText(window.getText() + 6);
    }

    @FXML
    void substract(ActionEvent event) {
        window.setText(window.getText() + "-");
    }

    @FXML
    void thee(ActionEvent event) {
        window.setText(window.getText() + 3);
    }

    @FXML
    void two(ActionEvent event) {
        window.setText(window.getText() + 2);
    }

    @FXML
    void division(ActionEvent event) {
        window.setText(window.getText() + "/");
    }

    @FXML
    void zero(ActionEvent event) {
        window.setText(window.getText() + 0);
    }

    /**
     * Метод для активации анимации при ошибках
     *
     */

    @FXML
    void checkWindow(ActionEvent event) {
        Shake windowAnim = new Shake(window);
        windowAnim.playAnimation();
        return;
    }

    /**
     * Метод для нахождения чисел введеных пользователем и знака операции
     *
     */

    @FXML
    ArrayList<String> searchNumbersAndOperand(ActionEvent event) {
        ArrayList<String> arrayExpression = new ArrayList<>();
        if (window.getText().isEmpty()) {
            checkWindow(event);
            return null;
        }
        String expression = window.getText();
        String stringNumbers = expression.replaceAll("[\\/ || \\+ || \\- || \\* ]", " ");

        String arrayNumbers[] = stringNumbers.split(" ");

        String stringOperations = expression.replaceAll("[0-9 \\.]+", " ");
        String operand = stringOperations.replaceAll("\\s+", "");

        if(arrayNumbers.length!=2 || operand.isEmpty() || (arrayNumbers.length!=2 && operand.isEmpty())) {
            checkWindow(event);
            return null;
        }
        String firstNumber = arrayNumbers[0];
        String secondNumber = arrayNumbers[1];

        arrayExpression = new ArrayList<>(Arrays.asList(firstNumber, secondNumber, operand));
        decision(arrayExpression);
        return arrayExpression;
    }

    /**
     * Метод выполняет выражение которое пользователь ввел в главное окно калькулятора,в зависимости от знака операции
     *
     */
    @FXML
    void decision(ArrayList<String> arrayExpression) {
        double result = 0;
        double firstNumber = Double.valueOf(arrayExpression.get(0));
        double secondNumber = Double.valueOf(arrayExpression.get(1));
        String operand = arrayExpression.get(2);

        switch (operand) {
            case "+":
                result = firstNumber + secondNumber;
                break;
            case "-":
                result = firstNumber - secondNumber;
                break;
            case "/":
                if (secondNumber != 0) {
                    result = firstNumber / secondNumber;
                } else {
                    window.setText("*ERROR*");
                    return;
                }
                break;

            case "*":
                result = firstNumber * secondNumber;
                break;
        }
        window.setText(String.valueOf(result));
    }
}