package Exam;

public class Exam {
    public static void main(String[] args) {
        String array[] = {"a", "dfgh", "gdfg", "fdh", "a"};
        print(sort(array));
    }

    private static String[] sort(String array[]) {
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array.length - 1; j++) {
                int arg = array[j].charAt(0) - array[j + 1].charAt(0);
                if (arg > 0) {
                    String temp = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = temp;
                }
            }
        }
        return array;
    }

    private static void print(String[] array) {
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + " ");
        }
    }
}
