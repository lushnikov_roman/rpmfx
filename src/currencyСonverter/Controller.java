package currencyСonverter;

import com.jfoenix.controls.JFXSlider;
import com.jfoenix.controls.JFXTextField;
import com.jfoenix.controls.JFXToggleButton;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import machine.animation.Shake;

public class Controller {

    @FXML
    private JFXToggleButton rounding;

    @FXML
    private JFXToggleButton rubles;

    @FXML
    private Button count;

    @FXML
    private Button clear;

    @FXML
    private JFXSlider SliderOfPercent;

    @FXML
    private ComboBox<?> currency;

    @FXML
    private JFXTextField amountOfMoney;

    @FXML
    private JFXTextField tips;

    @FXML
    private JFXTextField result;

    @FXML
    void clear(ActionEvent event) {
        amountOfMoney.clear();
        result.clear();
        tips.clear();
    }

    /**
     * Метод для подсчета чаевых
     * @param event
     * @return tips(чаевые)
     */

    @FXML
    double calculationTips(ActionEvent event) {
        double tipsPercent = SliderOfPercent.getValue() * 0.01;
        double tips=0;
        tips = Double.parseDouble(amountOfMoney.getText()) * tipsPercent;
        return tips;
    }

    /**
     * Метод для проверки полей,на введенные данные
     * @param event
     */

    @FXML
    void checkFields(ActionEvent event) {

        if (currency.getValue() == null) {
            Shake currencyBoxAnim = new Shake(currency);
            currencyBoxAnim.playAnimation();
            return;
        } else if (amountOfMoney.getText().isEmpty() || amountOfMoney.getText().matches("\\D.*") || Double.parseDouble(amountOfMoney.getText()) < 0) {
            Shake oldAmountAnim = new Shake(amountOfMoney);
            oldAmountAnim.playAnimation();
        } else {
            conversion(event);
        }
    }

    /**
     * Метод определяющий пожелания пользователя
     * @param event
     */

    void conversion(ActionEvent event) {
        double amount = Double.parseDouble(amountOfMoney.getText());
        double tipsCalc=calculationTips(event);
        if(rubles.isSelected()){
            amount=rubTransferSumm(event,amount);
            tipsCalc=rubTransferSumm(event,tipsCalc);
        }
        if (rounding.isSelected()){
            tipsCalc=roundingSumm(event,tipsCalc);
            amount=roundingSumm(event,amount);
        }else {
            amount=Math.floor(amount * 100) / 100 ;
            tipsCalc=Math.floor(tipsCalc * 100) / 100 ;
        }
        print(event, amount,tipsCalc);
    }

    /**
     * Метод для округления до целых
     * @param event
     * @param summ сумма для округления
     * @return вывод округленной суммы
     */

     double roundingSumm(ActionEvent event,double summ) {
         double roundingSumm=Math.ceil(summ);
         return roundingSumm;
    }

    /**
     * Метод перевода суммы в рубли
     * @param event
     * @param summ сумма для перевода
     * @return сумма в рублях
     */

    double rubTransferSumm(ActionEvent event, double summ) {
        if ("USD".equals(currency.getValue())) {
            summ = summ*75.129;
        }
        if ("EUR".equals(currency.getValue())) {
            summ = summ*81.1468;
        }
        return summ;
    }

    /**
     * Метод для вывода полученных результатов
     * @param event
     * @param amount итоговый счет
     * @param tip чаевые
     */


    void print(ActionEvent event,double amount,double tip) {
        tips.setText(tip + сurrencyIcon(event) );
        result.setText((amount+tip) + сurrencyIcon(event));
    }

    /**
     * Метод определения знака валюты
     * @param event
     * @return знака валюты
     */

    String сurrencyIcon(ActionEvent event) {
        String icon= "";
        if ("USD".equals(currency.getValue())) {
            icon= "$";
        }
        if ("EUR".equals(currency.getValue())) {
            icon= "€";
        }
        if ("RUR".equals(currency.getValue())) {
            icon= "₽";
        }
        if(rubles.isSelected()){
            icon= "₽";
        }
        return icon;
    }
}
