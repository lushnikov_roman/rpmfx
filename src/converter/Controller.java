package converter;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import machine.animation.Shake;

public class Controller {

    @FXML
    private ComboBox<?> secondComboBox;

    @FXML
    private ComboBox<?> firstComboBox;

    @FXML
    private TextField oldAmount;

    @FXML
    private TextField newAmount;

    @FXML
    private Button calculate;

    @FXML
    private Button clear;

    @FXML
    void clear(ActionEvent event) {
        oldAmount.clear();
        newAmount.clear();
    }

    /**
     * Метод проверки ComboBox и поля oldAmount
     * @param event
     */

    @FXML
    void checkFields(ActionEvent event) {
        if (secondComboBox.getValue() == null || firstComboBox.getValue() == null) {
            Shake firstComboBoxAnim = new Shake(firstComboBox);
            Shake secondComboBoxAnim = new Shake(secondComboBox);
            firstComboBoxAnim.playAnimation();
            secondComboBoxAnim.playAnimation();
            return;
        }else if(oldAmount.getText().isEmpty()||oldAmount.getText().matches("\\D.*")||Double.valueOf(oldAmount.getText())<0){
            Shake oldAmountAnim = new Shake(oldAmount);
            oldAmountAnim.playAnimation();
        }else {
            conversion(event);
        }
    }

    /**
     * Метод выполняющий конвертацию,в завивисимости от валюты
     * @param event
     */

    @FXML
    void conversion(ActionEvent event) {
        double result = Double.valueOf(oldAmount.getText());
        if ("RUR".equals(secondComboBox.getValue())) {
            if ("EUR".equals(firstComboBox.getValue())) {
                result /= 81.1468;
            }
            if ("USD".equals(firstComboBox.getValue())) {
                result /= 75.129;
            }
        }

        if ("USD".equals(secondComboBox.getValue())) {
            if ("EUR".equals(firstComboBox.getValue())) {
                result *= 0.9258;
            }
            if ("RUR".equals(firstComboBox.getValue())) {
                result *= 75.129;
            }
        }

        if ("EUR".equals(secondComboBox.getValue())) {
            if ("RUR".equals(firstComboBox.getValue())) {
                result *= 81.1468;
            }
            if ("USD".equals(firstComboBox.getValue())) {
                result *= 1.0801;
            }
        }
        newAmount.setText(String.valueOf(result));
    }

}

