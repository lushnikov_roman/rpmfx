package machine;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("arithmometer.fxml"));
        primaryStage.getIcons().add(new Image("machine/calc.jpg"));
        primaryStage.setTitle("Калькуляторчик");
        primaryStage.setScene(new Scene(root, 230, 303));
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
